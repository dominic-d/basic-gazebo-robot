#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
// 12*298 ticks per revolution
#define TICKS_PER_RADIAN (569.13807649661772070952833782011) 
#define WHEELBASE (0.125) // m
#define WHEEL_CIRCUMFERENCE (2 * 3.14159 * 0.02)

namespace gazebo
{
	class EdubotTeleopPlugin : public ModelPlugin
	{
		// Pointer to the model
		private: physics::ModelPtr model;
		// Pointer to the update event connection
		private: event::ConnectionPtr updateConnection;
		// Pointer to each wheel
		private: physics::JointPtr leftWheel;
		private: physics::JointPtr rightWheel;
		// ROS nodehandle and subscriber
		private: ros::NodeHandle nh;
		private: ros::Subscriber twistSubscriber;

		public: void OnTwist(geometry_msgs::TwistConstPtr twistMsg){
			ROS_INFO_NAMED("Teleop", "teleop got twist");

			double forwardSpeed = twistMsg->linear.x;
			double angularSpeed = twistMsg->angular.z;

			double vLeftMetres = (2 * forwardSpeed - WHEELBASE * angularSpeed) / 2;
			double vRightMetres = (2 * forwardSpeed + WHEELBASE * angularSpeed) / 2;

			double vLeftRadians = vLeftMetres / WHEEL_CIRCUMFERENCE * 2 * 3.14159;
			double vRightRadians = vRightMetres / WHEEL_CIRCUMFERENCE * 2 * 3.14159;

			// this->leftWheel->SetVelocity(0, vLeft);
			// this->rightWheel->SetVelocity(0, vRight);
			this->leftWheel->SetParam("vel", 0, vLeftRadians);
			this->rightWheel->SetParam("vel", 0, vRightRadians);
			ROS_INFO_STREAM_NAMED("Teleop", "teleop vel: " << vLeftMetres << ", " << vRightMetres);
		}

		public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
		{
			ROS_INFO_NAMED("Teleop", "loading Teleop plugin");
			// Store the pointer to the model
			this->model = _parent;

			this->leftWheel = _parent->GetJoint("left_wheel_joint");
			this->rightWheel = _parent->GetJoint("right_wheel_joint");

			this->leftWheel->SetParam("fmax", 0, 100.0);
			this->rightWheel->SetParam("fmax", 0, 100.0);

			if (!ros::isInitialized())
			{
				ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
				<< "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
				return;
			}

			this->twistSubscriber = nh.subscribe("/cmd_vel", 1, &EdubotTeleopPlugin::OnTwist, this);
			
			ROS_INFO_NAMED("Teleop", "loaded Teleop plugin");
		}


	};
	// Register this plugin with the simulator
	GZ_REGISTER_MODEL_PLUGIN(EdubotTeleopPlugin)
}