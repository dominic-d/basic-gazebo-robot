#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int32MultiArray.h>
// 12*298 ticks per revolution
#define TICKS_PER_RADIAN (569.13807649661772070952833782011) 

namespace gazebo
{
	class EdubotEncoderPlugin : public ModelPlugin
	{
		// Pointer to the model
		private: physics::ModelPtr model;
		// Pointer to the update event connection
		private: event::ConnectionPtr updateConnection;
		// Pointer to each wheel
		private: physics::JointPtr leftWheel;
		private: physics::JointPtr rightWheel;
		// ROS nodehandle and odometry publisher
		private: ros::NodeHandle nh;
		private: ros::Publisher encoderPublisher;

		public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
		{
			ROS_INFO_NAMED("Encoder", "loading Encoder plugin");
			// Store the pointer to the model
			this->model = _parent;

			this->leftWheel = _parent->GetJoint("left_wheel_joint");
			this->rightWheel = _parent->GetJoint("right_wheel_joint");

			if (!ros::isInitialized())
			{
				ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
				<< "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
				return;
			}

			this->encoderPublisher = nh.advertise<std_msgs::Int32MultiArray>("/edubot/encoders", 1);

			// Listen to the update event. This event is broadcast every
			// simulation iteration.
			this->updateConnection = event::Events::ConnectWorldUpdateBegin(
				std::bind(&EdubotEncoderPlugin::OnUpdate, this));
			
			ROS_INFO_NAMED("Encoder", "loaded Encoder plugin");
		}

		// Called by the world update start event
		public: void OnUpdate()
		{
			double leftRadians = this->leftWheel->Position(0);
			double rightRadians = this->rightWheel->Position(0);

			std_msgs::Int32MultiArray encoderMessage = std_msgs::Int32MultiArray();

			encoderMessage.layout.dim.push_back(std_msgs::MultiArrayDimension());
			encoderMessage.layout.dim[0].size = 2;
			encoderMessage.layout.dim[0].stride = 2;
			encoderMessage.layout.dim[0].label = "encoders";
			encoderMessage.data = {
				(int32_t) lroundf(leftRadians * TICKS_PER_RADIAN), 
				(int32_t) lroundf(rightRadians * TICKS_PER_RADIAN)
			};

			this->encoderPublisher.publish(encoderMessage);

			// ROS_INFO_STREAM_NAMED("Encoder", "(" << leftRadians << "," << rightRadians << ")");
		}

	};
	// Register this plugin with the simulator
	GZ_REGISTER_MODEL_PLUGIN(EdubotEncoderPlugin)
}