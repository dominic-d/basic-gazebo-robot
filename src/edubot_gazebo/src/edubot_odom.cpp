#include <ignition/math/Pose3.hh>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Int32MultiArray.h>
#include <tf/transform_broadcaster.h>

#define WHEEL_RADIUS (0.02) // m 
#define WHEELBASE (0.125) // m
// 12*298 ticks per revolution
#define TICKS_PER_RADIAN (569.13807649661772070952833782011) 


// Angle at last update step
double lastLeftRadians = 0.0;
double lastRightRadians = 0.0;
// Last pose of the robot
ignition::math::Pose3d lastPose;
// ROS pubs and subs
ros::Subscriber encoderSubscriber;
ros::Publisher odomPublisher;

// Called when encoder information is received
void updatePose(std_msgs::Int32MultiArrayConstPtr encodersMsg)
{
	double leftRadians = encodersMsg->data[0] / TICKS_PER_RADIAN;
	double rightRadians = encodersMsg->data[1] / TICKS_PER_RADIAN;

	double leftDist = (leftRadians - lastLeftRadians) * WHEEL_RADIUS;
	double rightDist = (rightRadians - lastRightRadians) * WHEEL_RADIUS;

	double xDist = (leftDist + rightDist) / 2;
	double thetaDist = (rightDist - leftDist) / WHEELBASE;

	double lastAngle = lastPose.Rot().Yaw();
	double newX = lastPose.Pos().X() + xDist * cos(lastAngle);
	double newY = lastPose.Pos().Y() + xDist * sin(lastAngle);
	double newTheta = lastAngle + thetaDist;
	ignition::math::Pose3d newPose = ignition::math::Pose3d(newX, newY, 0.0, 0.0, 0.0, newTheta);

	nav_msgs::Odometry odomMessage;
	odomMessage.pose.pose.position.x = newPose.Pos().X();
	odomMessage.pose.pose.position.y = newPose.Pos().Y();
	odomMessage.pose.pose.orientation.x = newPose.Rot().X();
	odomMessage.pose.pose.orientation.y = newPose.Rot().Y();
	odomMessage.pose.pose.orientation.z = newPose.Rot().Z();
	odomMessage.pose.pose.orientation.w = newPose.Rot().W();
	odomMessage.header.stamp = ros::Time::now();
	odomMessage.header.frame_id = "odom";
	odomMessage.child_frame_id = "base_link";

	odomPublisher.publish(odomMessage);

	static tf::TransformBroadcaster tfBroadcaster;
	tf::Transform robotPoseTf;
	robotPoseTf.setOrigin( tf::Vector3(newPose.Pos().X(), newPose.Pos().Y(), newPose.Pos().Z()) );
	robotPoseTf.setRotation(
		tf::Quaternion(newPose.Rot().X(), newPose.Rot().Y(), newPose.Rot().Z(), newPose.Rot().W()));
	tfBroadcaster.sendTransform(tf::StampedTransform(robotPoseTf, ros::Time::now(), "odom", "base_link"));

	lastLeftRadians = leftRadians;
	lastRightRadians = rightRadians;
	lastPose = newPose;
}

void initOdom()
{
	ROS_INFO_NAMED("Odometry", "loading Odometry node");
	
	// init
	lastPose = ignition::math::Pose3d();

	ros::NodeHandle nh;
	encoderSubscriber = nh.subscribe("/edubot/encoders", 1, updatePose);
	odomPublisher = nh.advertise<nav_msgs::Odometry>("/edubot/odom", 1);

	ROS_INFO_NAMED("Odometry", "loaded Odometry node");
}

int main(int argc, char **argv){
	ros::init(argc, argv, "odometryIntegration");

	// tf::TransformBroadcaster tfBroadcaster;

	initOdom();

	ros::spin();
}