#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
// 12*298 ticks per revolution
#define TICKS_PER_RADIAN (569.13807649661772070952833782011) 

namespace gazebo
{
	class EdubotOdomTruePlugin : public ModelPlugin
	{
		// Pointer to the model
		private: physics::ModelPtr model;
		// Pointer to the update event connection
		private: event::ConnectionPtr updateConnection;
		// Pointer to each wheel
		private: physics::LinkPtr baseLink;
		// ROS nodehandle and odometry publisher
		private: ros::NodeHandle nh;
		private: ros::Publisher odometryPublisher;
		// tf broadcaster
		private: tf::TransformBroadcaster tfBroadcaster;

		public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
		{
			ROS_INFO_NAMED("True Odometry", "loading True Odometry plugin");
			// Store the pointer to the model
			this->model = _parent;

			this->baseLink = _parent->GetLink("base_link");

			if (!ros::isInitialized())
			{
				ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
				<< "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
				return;
			}

			this->odometryPublisher = nh.advertise<nav_msgs::Odometry>("/edubot/odometry_true", 1);

			// Listen to the update event. This event is broadcast every
			// simulation iteration.
			this->updateConnection = event::Events::ConnectWorldUpdateBegin(
				std::bind(&EdubotOdomTruePlugin::OnUpdate, this));
			
			ROS_INFO_NAMED("True Odometry", "loaded True Odometry plugin");
		}

		// Called by the world update start event
		public: void OnUpdate()
		{
			ignition::math::Pose3d basePose = this->baseLink->WorldCoGPose();

			nav_msgs::Odometry odometryMessage = nav_msgs::Odometry();

			odometryMessage.header.stamp = ros::Time::now();
			odometryMessage.header.frame_id = "map";
			odometryMessage.child_frame_id = "base_link";
			odometryMessage.pose.pose.position.x = basePose.Pos().X();
			odometryMessage.pose.pose.position.y = basePose.Pos().Y();
			odometryMessage.pose.pose.position.z = basePose.Pos().Z();
			odometryMessage.pose.pose.orientation.w = basePose.Rot().W();
			odometryMessage.pose.pose.orientation.x = basePose.Rot().X();
			odometryMessage.pose.pose.orientation.y = basePose.Rot().Y();
			odometryMessage.pose.pose.orientation.z = basePose.Rot().Z();

			this->odometryPublisher.publish(odometryMessage);

			// tf::Transform robotPoseTf;
			// robotPoseTf.setOrigin( tf::Vector3(basePose.pos.x, basePose.pos.y, basePose.pos.z) );
			// robotPoseTf.setRotation(
			// 	tf::Quaternion(basePose.rot.x, basePose.rot.y, basePose.rot.z, basePose.rot.w));
			// tfBroadcaster.sendTransform(tf::StampedTransform(robotPoseTf, ros::Time::now(), "odom", "base_link"));

		}

	};
	// Register this plugin with the simulator
	GZ_REGISTER_MODEL_PLUGIN(EdubotOdomTruePlugin)
}