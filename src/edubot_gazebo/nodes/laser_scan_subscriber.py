#!/usr/bin/python

import rospy
import math
import time
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import tf_conversions

class laser_scan_subscriber:

	def __init__(self):
		self.setup_parameters()
		self.setup_publishers()
		self.setup_subscribers()

	def setup_parameters(self):
		self.last_odom = None

	def setup_publishers(self):	
		self.pub_test = rospy.Publisher("/edubot/unrotated_laser_scan", LaserScan, queue_size=1)

	def setup_subscribers(self):
		self.sub_odom = rospy.Subscriber("/edubot/odom", Odometry, self.process_odom_data, queue_size=1)
		self.sub_images = rospy.Subscriber('/edubot/laser/scan', LaserScan, self.process_laser_data, queue_size=1)
		# LaserScan docs - http://docs.ros.org/melodic/api/sensor_msgs/html/msg/LaserScan.html
		# Odometry docs - http://docs.ros.org/melodic/api/nav_msgs/html/msg/Odometry.html

	def process_odom_data(self, msg):
		self.last_odom = msg

	def process_laser_data(self, msg):
		if self.last_odom is not None:
			# use a builtin library to do the confusing quaternion maths for us
			robot_angle = tf_conversions.fromMsg(self.last_odom.pose.pose).M.GetRPY()[2] # rad
			# we want to rotate the laser scan by this angle so it's aligned with the world
			laser_points_to_move = int(round(robot_angle / msg.angle_increment))
			new_laser_ranges = msg.ranges[-laser_points_to_move:] + msg.ranges[:-laser_points_to_move]

			# copy the data from the old message, but change the ranges
			new_laser_msg = msg
			new_laser_msg.ranges = new_laser_ranges
			# change the frame of the laser scan so we know it's with respect to the world, not the robot
			new_laser_msg.header.frame_id = self.last_odom.header.frame_id

			self.publish_laser_message(new_laser_msg)
		
	def publish_laser_message(self, laser_msg):
		self.pub_test.publish(laser_msg)


if __name__ == "__main__":
	# register the name of the node (you can then find it going rosnode list)
	rospy.init_node("laser_scan_subscriber")
	# create the node class which handles everything
	node = laser_scan_subscriber()
	# spin keeps this process open so the subscribers can receive the incoming messages
	rospy.spin()