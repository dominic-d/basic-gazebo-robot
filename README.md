# Basic gazebo robot sim

## Install 

For the basic robot:

```bash
sudo apt install ros-<distro>-gmapping
sudo apt install ros-<distro>-gazebo-ros-pkgs
sudo apt install ros-<distro>-teleop-twist-keyboard
```

For autonomous exploration: (explore-lite may not exist on newer ROS versions)

```bash
sudo apt install ros-<distro>-move-base
sudo apt install ros-<distro>-explore-lite
```

## Run

Run the following in this directory.

```bash
catkin build
source devel/setup.bash
roslaunch edubot_gazebo edubot.launch
```

You can then teleop the robot in the simulation (I = forwards, K = stop, J = left, L = right, , = backwards) if your cursor is focused on the terminal window that you launched the simulation with.

You can also run autonomous exploration in another terminal:

```bash
roslaunch edubot_nav edubot_nav.launch
```
